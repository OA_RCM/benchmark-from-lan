#!/bin/bash

#####################################################
# Script to move code to Unix and trigger Mover.ksh #
#####################################################
# example for omni
# upon jenkins polling change in repo, this is initiated.

#check for cycle 1pm - 2pm
time=`date +%H`
if test $time -eq 13  
then
	exit  ## maybe put in sleep command for 2 hours instead - or 30 min increments???  sleep 2 [h] then retest
	echo "Cycle is running. Cannot move code."
	echo $?
else
	if test $time -eq 14
	then
		exit ## maybe put in sleep command for 1 hour???
		echo "Cycle is running. Cannot move code."
		echo $?
	else
		echo "No cycle in process. Now code will be moved."
	fi
fi
# might change to a loop if sleep is used


# ID branch that changed
# move code to SUTOMNI01

# when executing shell run like: this_script.sh -label $GIT_BRANCH to call as param
# according to jenkins documentation
declare -a branchArr=("funct" "release")
for $GIT_BRANCH in "${branchArr[@]}"
	cp /src_code/x_program/ /omnitest/deploys/funct/ #copy new code to deploy dirs
	# Trigger Mover.ksh
	SSH_Run = "ssh $user@SUTOMNI01"  # need username
	$SSH_Run "korn mover.ksh"  # not sure "korn" is correct usage
done









# for GIT_BRANCH = 'funct'
	# cp /src_code/x_program/ /omnitest/deploys/funct/ #copy new code to deploy dirs
	Trigger Mover.ksh
	# SSH_Run = "ssh $user@SUTOMNI01"  # need username
	# $SSH_Run "korn mover.ksh"  # not sure "korn" is correct usage
# done
	# if GIT_BRANCH = 'release'
		# cp /src_code/x_program/ /omnitest/deploys/release/



#Trigger Mover.ksh
#SSH_Run = "ssh $user@host"
#$SSH_Run "korn mover.ksh"